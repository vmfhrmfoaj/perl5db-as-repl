;;; perl5db-as-repl.el --- Using Perl5 debugger as REPL with Emacs -*- lexical-binding: t -*-

;; Copyright (C) 2018 Jinseop Kim <vmfhrmfoaj@yahoo.com>

;; Author: Jinseop Kim <vmfhrmfoaj@yahoo.com>
;; Package-Requires: ((emacs "24.3") (dash "2.6.0"))
;; Keywords: perl5 repl
;; URL: https://gitlab.com/vmfhrmfoaj/perl5db-as-repl
;; Version: 0.0.0

;; This program is free software; you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation; either version 2 of the License, or (at your option) any later
;; version.

;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
;; details.

;; You should have received a copy of the GNU General Public License along with
;; this program; if not, write to the Free Software Foundation, Inc., 51
;; Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

;;; Commentary:

;; TODO

;;; Code:

(require 'dash)
(require 'perl-mode)

(defcustom perl5db-as-repl/buf-fmt
  "*perl5db-as-repl: %s*"
  "TODO"
  :type 'string
  :safe 'stringp)

(defface perl5db-as-repl-prompt-face
  '((t (:inherit font-lock-keyword-face)))
  "Face for the prompt in the REPL buffer."
  :group 'perl5db-as-repl)

(setq perl5db-as-repl//servers nil)     ; ((port . server) ...)
(setq perl5db-as-repl//clients nil)     ; ((server . client) ...)
(setq perl5db-as-repl//callbacks nil)   ; ((client . fn) ...)
(setq perl5db-as-repl//port nil)
(make-local-variable 'perl5db-as-repl//port)

(defvar perl5db-as-repl/init-cmds '("select $DB::OUT")
  "TODO")

(defvar perl5db-as-repl/default-input-function #'perl5db-as-repl//insert
  "TODO")

(defvar perl5db-as-repl/prompt-regex "^[ ]+DB<[0-9]+> "
  "TODO")

(defun perl5db-as-repl//server-start (port)
  "TODO"
  (interactive "nPort: ")
  (add-to-list
   'perl5db-as-repl//servers
   (let ((server (make-network-process
                  :name (concat "perl5db:" (number-to-string port))
                  :family 'ipv4
                  :host 'local
                  :service port
                  :filter-multibyte nil
                  :server t
                  :noquery t)))
     (set-process-filter server (-partial #'perl5db-as-repl//server-cb server))
     (cons port server)))
  port)

(defun perl5db-as-repl//server-stop (port)
  (-when-let (server (alist-get port perl5db-as-repl//servers))
    (-when-let (client (alist-get server perl5db-as-repl//clients))
      (delete-process client)
      (->> perl5db-as-repl//clients
           (--remove (equal server (car it)))
           (setq perl5db-as-repl//clients)))
    (delete-process server)
    (->> perl5db-as-repl//servers
         (--remove (equal port (car it)))
         (setq perl5db-as-repl//servers))
    port))

(defun perl5db-as-repl//server-cb (server client msg)
  (let ((_client (alist-get server perl5db-as-repl//clients)) cb-fired?)
    (unless (eq client _client)
      (add-to-list 'perl5db-as-repl//clients (cons server client))
      (dolist (cmd perl5db-as-repl/init-cmds)
        (process-send-string client (concat cmd "\n"))))
    (->> perl5db-as-repl//callbacks
         (--map (if (equal client (car it))
                    (let ((cb (cdr it)))
                      (when (functionp cb)
                        (funcall cb msg))
                      (setq cb-fired? t)
                      nil)
                  it))
         (-non-nil)
         (setq perl5db-as-repl//callbacks))
    (when (and (not cb-fired?)
               perl5db-as-repl/default-input-function)
      (let* ((port (rassoc server perl5db-as-repl//servers))
             (buf (perl5db-as-repl//repl-buffer port)))
        (funcall perl5db-as-repl/default-input-function buf msg)))))

(defun perl5db-as-repl//send (port str &optional cb)
  (-when-let (client (-some-> port
                              (alist-get perl5db-as-repl//servers)
                              (alist-get perl5db-as-repl//clients)))
    (let (resp)
      (add-to-list 'perl5db-as-repl//callbacks (cons client (or cb (lambda (r) (setq resp r)))))
      (process-send-string client (concat (replace-regexp-in-string "\n" " " str) "\n"))
      (while (not (or cb resp))
        (accept-process-output client 0.1))
      (or cb resp))))

(defun perl5db-as-repl//prompt-point ()
  (save-excursion
    (goto-char (point-max))
    (when (re-search-backward perl5db-as-repl/prompt-regex nil t)
      (+ (point) (length (match-string 0))))))

(defun perl5db-as-repl//insert (buf &rest args)
  (let ((buf (or buf (current-buffer)))
        (str (apply #'concat args))
        (pos (point)))
    (with-current-buffer buf
      (insert str)
      (save-excursion
        (goto-char pos)
        (while (re-search-forward perl5db-as-repl/prompt-regex nil 'noerr)
          (set-text-properties (match-beginning 0) (match-end 0)
                               (list 'font-lock-face 'perl5db-as-repl-prompt-face
                                     'read-only t
                                     'intangible t
                                     'rear-nonsticky '(field read-only font-lock-face intangible))))))))

(defun perl5db-as-repl//repl-buffer-name (port)
  (format perl5db-as-repl/buf-fmt (if (numberp port)
                                      (number-to-string port)
                                    port)))

(defun perl5db-as-repl//repl-buffer (port &optional create-if-need)
  (let ((buf-name (perl5db-as-repl//repl-buffer-name port))
        (fn (if create-if-need #'get-buffer-create #'get-buffer)))
    (funcall fn buf-name)))

(defun perl5db-as-repl/eval-last-expression ()
  (interactive)
  (let* ((prompt-pos (or (perl5db-as-repl//prompt-point) 0))
         (last-exp (buffer-substring-no-properties prompt-pos (point-max))))
    (perl5db-as-repl//insert nil "\n")
    (perl5db-as-repl//send perl5db-as-repl//port
                           last-exp
                           (-partial #'perl5db-as-repl//insert (current-buffer)))))

(defvar perl5db-as-repl-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "RET") #'perl5db-as-repl/eval-last-expression)
    map)
  "TODO")

(define-derived-mode perl5db-as-repl-mode perl-mode "Perl5 Debugger as REPL"
  "TODO

\\{perl5db-as-repl-mode-map}"
  (electric-indent-mode -1))

(defun perl5db-as-repl/create-repl-listener (port)
  "TODO"
  (interactive "nPort: ")
  (let ((buf (perl5db-as-repl//repl-buffer port t)))
    (with-current-buffer buf
      (perl5db-as-repl-mode)
      (perl5db-as-repl//server-start port)
      (setq-local perl5db-as-repl//port port)
      (add-hook 'kill-buffer-hook
                (lambda ()
                  (perl5db-as-repl//server-stop perl5db-as-repl//port))
                nil 'local))
    (switch-to-buffer buf)))

(defun perl5db-as-repl/switch-to-repl ()
  "TODO"
  )

(provide 'perl5db-as-repl)

;;; perl5db-as-repl.el ends here
